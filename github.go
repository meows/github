package main

import (
   "encoding/json"
   "fmt"
   "io/ioutil"
   "net/http"
   "time"
)

// TaskUnit : extracted from nested tasks map
type TaskUnit struct {
   repository string // => "rust-lang"
   category   string // => "commits"
   url        string // => "https://api.example.com/endpoint"
}

// Receipt : TaskUnit{} -> fetch() -> Receipt
type Receipt struct {
   repository string
   category   string
   payload    interface{} // unmarshalled result of http.Get()
}

// General : Receipt{} -> decodeJSON() -> General{}
type General struct {
   ID       int    `json:"id"`
   Name     string `json:"name"`
   FullName string `json:"full_name"`
   Created  string `json:"created_at"`
   Updated  string `json:"updated_at"`
   Pushed   string `json:"pushed_at"`
   Watchers int    `json:"watchers"`
   Forks    int    `json:"forks"`
   Issues   int    `json:"open_issues"`
   Size     int    `json:"size"`
}

// Commits : objects found in array received from github api
type Commits struct {
   Days  []Day `json:"days"`
   Total int   `json:"total"`
   Week  int   `json:"week"`
}

// Day : days of the week
type Day struct {
   day1 int
   day2 int
   day3 int
   day4 int
   day5 int
   day6 int
   day7 int
}

func main() {
   var (
      initial []TaskUnit
      final   []Receipt
      queue   = make(chan Receipt)
   )

   // nested map of tasks
   tasks := map[string]map[string]string{
      "rust": {
         "general": "https://api.github.com/repos/rust-lang/rust",
         "commits": "https://api.github.com/repos/rust-lang/rust/stats/commit_activity",
      },
      "go": {
         "general": "https://api.github.com/repos/golang/go",
         "commits": "https://api.github.com/repos/golang/go/stats/commit_activity",
      },
   }

   // unpack tasks into TaskUnit{} and append to initial[]
   for repository := range tasks {
      for category, url := range tasks[repository] {
         initial = append(initial, TaskUnit{repository, category, url})
      }
   }

   for _, structure := range initial {
      go fetch(structure, queue)
   }

   for range initial {
      final = append(final, <-queue)
   }

   fmt.Println(initial)
   fmt.Println()
   fmt.Println(final)
}

func fetch(taskunit TaskUnit, queue chan<- Receipt) {
   response, _ := http.Get(taskunit.url)
   raw, _      := ioutil.ReadAll(response.Body)
   response.Body.Close()

   // result := decodeJSON(raw, taskunit)

   result := decodeCommits(raw)

   queue <- Receipt{taskunit.repository, taskunit.category, result}
}

func decodeJSON(raw []byte, taskunit TaskUnit) interface{} {
   switch taskunit.category {
      case "general":
         var result General
         json.Unmarshal(raw, &result)
         return result
      case "commits":
         var result []Commits
         json.Unmarshal(raw, &result)
         return result
      default:
         return "unidentified type"
   }
}

func decodeGeneral(raw []byte) General {
   var result General
   json.Unmarshal(raw, &result)
   return result
}

func decodeCommits(raw []byte) []Commits {
   var result []Commits
   json.Unmarshal(raw, &result)
   return result
}

func unixTimeDecoder(rawTime int64) interface{} {
   result := time.Unix(rawTime, 0)
   return result
}
